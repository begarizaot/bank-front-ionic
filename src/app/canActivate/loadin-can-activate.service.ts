import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root',
})
export class LoadinCanActivateService implements CanActivate {
  constructor(private navController: NavController, private storage: Storage) {}

  async canActivate(route: ActivatedRouteSnapshot) {
    const page = await this.storage.get('usuario');
    if (!page) {
      return true;
    }
    this.navController.navigateRoot(['/tabs']);
    return false;
  }
}
