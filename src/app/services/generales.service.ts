import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root',
})
export class GeneralesServService {
  constructor(private http: HttpClient) {}

  getTiposIdentificacion = () => {
    return this.http.get<any>(`${apiUrl}/api/general/tiposidentificacion`);
  };

  getEntidadesBancarias = () => {
    return this.http.get<any>(`${apiUrl}/api/general/entidadesBancarias`);
  };

  getTiposDeCuenta = () => {
    return this.http.get<any>(`${apiUrl}/api/general/tiposCuenta`);
  };

  getMonedas = () => {
    return this.http.get<any>(`${apiUrl}/api/general/monedas`);
  };
}
