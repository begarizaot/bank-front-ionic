import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root',
})
export class TransferenciasServService {
  constructor(private http: HttpClient) {}

  postTrasferencia = (data) => {
    return this.http.post<any>(
      `${apiUrl}/api/transferencias/crearTransferancia`,
      data
    );
  };

  getTrasferenciaByCuenta = (cuenta) => {
    return this.http.get<any>(
      `${apiUrl}/api/transferencias/cuentaTransferenciaByCuenta/${cuenta}`
    );
  };

  getTrasferenciaByIdCuenta = (idCuenta) => {
    return this.http.get<any>(
      `${apiUrl}/api/transferencias/cuentaByIdCuenta/${idCuenta}`
    );
  };
}
