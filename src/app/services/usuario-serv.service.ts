import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root',
})
export class UsuarioServService {
  constructor(private http: HttpClient) {}

  postLogin = (data) => {
    return this.http.post<any>(`${apiUrl}/api/usuario/login`, data);
  };
}
