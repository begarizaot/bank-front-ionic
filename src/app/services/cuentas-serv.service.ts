import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root',
})
export class CuentasServService {
  constructor(private http: HttpClient) {}

  postCrearCuentaTercero = (data) => {
    return this.http.post<any>(`${apiUrl}/api/cuenta/crearCuenta`, data);
  };

  getCuentasByIdUsuario = (id) => {
    return this.http.get<any>(`${apiUrl}/api/cuenta/cuentaUsuario/${id}`);
  };

  getCuentasById = (id) => {
    return this.http.get<any>(`${apiUrl}/api/cuenta/cuentabyId/${id}`);
  };
}
