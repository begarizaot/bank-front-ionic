const isValidEmail = (value: any) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(value).toLowerCase());
};

const validateEmail = (value: any) => {
  if (value == '') {
    return '';
  } else if (isValidEmail(value)) {
    return '';
  } else {
    return 'Email invalido';
  }
};

const validatePassword = (value: any) => {
  if (value.length < 9) {
    return 'La contraseña debe tener 9 caracteres';
  } else {
    return '';
  }
};

const validateInput = (value: any, minLength: any) => {
  if (value.length < minLength) {
    return `debe tener minimo ${minLength} caracteres`;
  } else {
    return '';
  }
};

const validateNumber = (value: any, ignorarPunt?: any) => {
  if (isNaN(value) || (!ignorarPunt && value.includes('.'))) {
    return 'Solo se permiten caracteres numéricos';
  } else {
    return '';
  }
};

const formatoPuntos = (x: any) => {
  x = String(x);
  if (!x) {
    return '';
  }
  const res = eliminarPuntos(x.replace(/[^.0-9]/g, ''))
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  return res;
};

const eliminarPuntos = (x: any) => {
  x = String(x);
  if (!x) {
    return '';
  }
  const res = x.replace(/[^.0-9]/g, '').toString().replace(/\./g, '');
  return res;
};

export {
  validateEmail,
  validatePassword,
  validateInput,
  validateNumber,
  formatoPuntos,
  eliminarPuntos,
};
