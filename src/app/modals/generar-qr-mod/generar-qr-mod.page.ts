import { Component, Input, OnInit } from '@angular/core';
import { GeneralesService } from 'src/app/controllers/generales.service';

import { formatoPuntos } from 'src/app/utils/utils';

@Component({
  selector: 'app-generar-qr-mod',
  templateUrl: './generar-qr-mod.page.html',
  styleUrls: ['./generar-qr-mod.page.scss'],
})
export class GenerarQrModPage implements OnInit {
  @Input() cuenta: any = {};

  objectGenerarQr: any = {};
  objectError: any = {};

  qrInfo: any = null;

  listMonedas: any = [];

  constructor(private generalesService: GeneralesService) {}

  ngOnInit() {
    this.getMonedas();
  }

  getMonedas = async () => {
    this.listMonedas = await this.generalesService.getMonedas();
  };

  // changes
  changeValidarValorTrasferir = ({ target: { value } }) => {
    this.objectGenerarQr.valorTraf = formatoPuntos(value);
  };

  // onPress
  onGenerarQr = (arg) => {
    if (!this.objectGenerarQr.moneda) {
      this.objectGenerarQr.isInvalido = true;
      return;
    }

    this.qrInfo = JSON.stringify({ ...arg, id: this.cuenta.id });
  };

  onAtras = () => {
    this.qrInfo = null;
  };
}
