import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GenerarQrModPageRoutingModule } from './generar-qr-mod-routing.module';

import { GenerarQrModPage } from './generar-qr-mod.page';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GenerarQrModPageRoutingModule,
    QRCodeModule,
  ],
  declarations: [GenerarQrModPage],
})
export class GenerarQrModPageModule {}
