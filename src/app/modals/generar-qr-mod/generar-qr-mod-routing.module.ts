import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GenerarQrModPage } from './generar-qr-mod.page';

const routes: Routes = [
  {
    path: '',
    component: GenerarQrModPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GenerarQrModPageRoutingModule {}
