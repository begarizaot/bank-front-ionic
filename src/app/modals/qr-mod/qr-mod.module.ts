import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QrModPageRoutingModule } from './qr-mod-routing.module';

import { QrModPage } from './qr-mod.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QrModPageRoutingModule
  ],
  declarations: [QrModPage]
})
export class QrModPageModule {}
