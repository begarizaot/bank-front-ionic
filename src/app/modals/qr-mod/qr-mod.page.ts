import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-qr-mod',
  templateUrl: './qr-mod.page.html',
  styleUrls: ['./qr-mod.page.scss'],
})
export class QrModPage implements OnInit {
  constructor(
    private navController: NavController,
    private modalController: ModalController,
    private barcodeScanner: BarcodeScanner
  ) {}

  ngOnInit() {}

  onGenerarQr = () => {
    this.modalController.dismiss();
    this.navController.navigateForward('generarQr');
  };

  onScanearQr = () => {
    this.barcodeScanner
      .scan()
      .then((barcodeData) => {
        const data: any = JSON.parse(barcodeData.text);
        this.modalController.dismiss();
        this.navController.navigateForward('/transferir/todos', {
          queryParams: data,
        });
      })
      .catch((err) => {
        console.log('Error', err);
      });
  };
}
