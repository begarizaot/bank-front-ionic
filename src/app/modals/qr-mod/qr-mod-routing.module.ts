import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QrModPage } from './qr-mod.page';

const routes: Routes = [
  {
    path: '',
    component: QrModPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QrModPageRoutingModule {}
