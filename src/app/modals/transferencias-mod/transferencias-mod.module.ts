import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransferenciasModPageRoutingModule } from './transferencias-mod-routing.module';

import { TransferenciasModPage } from './transferencias-mod.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransferenciasModPageRoutingModule
  ],
  declarations: [TransferenciasModPage]
})
export class TransferenciasModPageModule {}
