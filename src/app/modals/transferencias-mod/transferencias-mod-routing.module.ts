import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransferenciasModPage } from './transferencias-mod.page';

const routes: Routes = [
  {
    path: '',
    component: TransferenciasModPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransferenciasModPageRoutingModule {}
