import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-transferencias-mod',
  templateUrl: './transferencias-mod.page.html',
  styleUrls: ['./transferencias-mod.page.scss'],
})
export class TransferenciasModPage implements OnInit {
  constructor(
    private navController: NavController,
    private modalController: ModalController
  ) {}

  ngOnInit() {}

  onNavegacion = (arg) => {
    this.modalController.dismiss();
    this.navController.navigateForward(`/transferencias/${arg}`);
  };
}
