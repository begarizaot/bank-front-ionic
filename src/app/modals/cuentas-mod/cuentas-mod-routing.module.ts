import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuentasModPage } from './cuentas-mod.page';

const routes: Routes = [
  {
    path: '',
    component: CuentasModPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CuentasModPageRoutingModule {}
