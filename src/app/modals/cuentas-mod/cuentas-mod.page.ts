import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CuentasService } from 'src/app/controllers/cuentas.service';

@Component({
  selector: 'app-cuentas-mod',
  templateUrl: './cuentas-mod.page.html',
  styleUrls: ['./cuentas-mod.page.scss'],
})
export class CuentasModPage implements OnInit {
  @Input() estado: any = '';
  @Input() cuenta: any = '';
  @Input() idCuenta: any = '';

  constructor(
    public cuentasService: CuentasService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.cuentasService.getListadoDeCuentas();
  }

  onSeleccionarCuenta = (arg) => {
    this.modalController.dismiss(arg);
  };
}
