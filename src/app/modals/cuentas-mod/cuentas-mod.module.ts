import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuentasModPageRoutingModule } from './cuentas-mod-routing.module';

import { CuentasModPage } from './cuentas-mod.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CuentasModPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [CuentasModPage],
})
export class CuentasModPageModule {}
