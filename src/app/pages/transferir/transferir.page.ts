import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  LoadingController,
  ModalController,
  NavController,
} from '@ionic/angular';
import { CuentasService } from 'src/app/controllers/cuentas.service';
import { GeneralesService } from 'src/app/controllers/generales.service';
import { TransferenciasService } from 'src/app/controllers/transferencias.service';
import { CuentasModPage } from 'src/app/modals/cuentas-mod/cuentas-mod.page';
import { UsuarioStorageService } from 'src/app/storages/usuario-storage.service';

import { eliminarPuntos, formatoPuntos } from 'src/app/utils/utils';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-transferir',
  templateUrl: './transferir.page.html',
  styleUrls: ['./transferir.page.scss'],
})
export class TransferirPage implements OnInit {
  cuenta: any = '';

  objectTrasferencia: any = {};
  objectError: any = {};

  listMonedas: any = [];

  objectUsuario: any = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private modalController: ModalController,
    private generalesService: GeneralesService,
    private cuentasService: CuentasService,
    private loadingController: LoadingController,
    private usuarioStorage: UsuarioStorageService,
    private navController: NavController,
    private transferenciasService: TransferenciasService
  ) {}

  ngOnInit() {
    this.storageCargarUsuario();
    this.cuenta = this.activatedRoute.snapshot.paramMap.get('cuenta');
    this.activatedRoute.queryParamMap.subscribe(
      async ({ params: { id, moneda, valorTraf } }: any) => {
        // const destino = this.cuentasService.cuentas.find((inf) => inf.id == id);

        this.objectTrasferencia = {
          moneda: Number(moneda),
          valorTraf,
          qr: id ? true : false,
          destino: id && (await this.cuentasService.getCuentasById(id)),
        };

        this.getMonedas();
      }
    );
  }

  storageCargarUsuario = async () => {
    this.objectUsuario = await this.usuarioStorage.storageCargarUsuario();
  };

  getMonedas = async () => {
    this.listMonedas = await this.generalesService.getMonedas();
  };

  // changes
  changeValidarValorTrasferir = ({ target: { value } }) => {
    this.objectTrasferencia.valorTraf = formatoPuntos(value);
  };

  // onpress

  onSeleccionarCuenta = async (arg) => {
    const cuentaMod = await this.modalController.create({
      component: CuentasModPage,
      cssClass: 'cuentaMod',
      mode: 'ios',
      swipeToClose: true,
      componentProps: {
        cuenta: this.cuenta,
        estado: arg,
        idCuenta:
          this.objectTrasferencia?.destino?.id ||
          this.objectTrasferencia?.origen?.id,
      },
    });
    await cuentaMod.present();

    const { data } = await cuentaMod.onWillDismiss();

    data && (this.objectTrasferencia[arg] = data);
  };

  onGenerarTransferencia = async (arg) => {
    if (this.handleValidarInformacionCompleta()) {
      this.objectTrasferencia.isInvalido = true;
      return;
    }

    const data = {
      ...arg,
      valorTraf: Number(eliminarPuntos(arg.valorTraf)),
      cuenta: this.cuenta,
    };
    if (data.valorTraf > data.origen.saldo) {
      Swal.fire({
        icon: 'info',
        text: 'El valor no debe ser mayor al valor de origen',
        showConfirmButton: false,
      });
      return;
    }

    if (data.origen.id == data.destino.id) {
      Swal.fire({
        icon: 'info',
        text: 'La cuenta de origen y destino no deben ser las mismas',
        showConfirmButton: false,
      });
      return;
    }

    const loading = await this.loadingController.create({
      message: 'Generando Trasferencia',
    });
    await loading.present();

    this.transferenciasService
      .postTrasferencia({ ...data, id_usuario: this.objectUsuario.id })
      .then((res) => {
        loading.dismiss();
        Swal.fire({
          icon: 'success',
          text: 'Se ha agregado exitosamente',
          showConfirmButton: false,
        });
        this.transferenciasService.getTrasferenciaByCuenta(this.cuenta);
        this.navController.back();
      })
      .catch(({ db, messenger }) => {
        loading.dismiss();
        Swal.fire({
          icon: db ? 'error' : 'info',
          text: messenger,
          showConfirmButton: false,
        });
      });
  };

  // handles
  handleValidarInformacionCompleta = () => {
    return (
      !this.objectTrasferencia?.destino?.id ||
      !this.objectTrasferencia.moneda ||
      !this.objectTrasferencia.origen.id ||
      !this.objectTrasferencia.valorTraf
    );
  };
}
