import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CuentasService } from 'src/app/controllers/cuentas.service';
import { GenerarQrModPage } from 'src/app/modals/generar-qr-mod/generar-qr-mod.page';

@Component({
  selector: 'app-generar-qr',
  templateUrl: './generar-qr.page.html',
  styleUrls: ['./generar-qr.page.scss'],
})
export class GenerarQrPage implements OnInit {
  constructor(
    public cuentasService: CuentasService,
    private modalController: ModalController
  ) {}

  ngOnInit() {}

  onGenerarQr = async (arg) => {
    const generarQr = await this.modalController.create({
      component: GenerarQrModPage,
      cssClass: 'generarQr',
      mode: 'md',
      componentProps: { cuenta: arg },
    });
    await generarQr.present();

    const { data } = await generarQr.onWillDismiss();
  };
}
