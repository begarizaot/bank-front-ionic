import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, ModalController } from '@ionic/angular';
import { CuentasService } from 'src/app/controllers/cuentas.service';
import { TransferenciasService } from 'src/app/controllers/transferencias.service';
import { GenerarQrModPage } from 'src/app/modals/generar-qr-mod/generar-qr-mod.page';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-detalle-cuenta',
  templateUrl: './detalle-cuenta.page.html',
  styleUrls: ['./detalle-cuenta.page.scss'],
})
export class DetalleCuentaPage implements OnInit {
  objectDetalle: any = {};
  objectTrasferencias: any = [];

  alias: any = '';
  isUpdatAlias: boolean = false;

  constructor(
    public loadingController: LoadingController,
    private modalController: ModalController,
    private cuentasService: CuentasService,
    private activatedRoute: ActivatedRoute,
    private transferenciasService: TransferenciasService
  ) {}

  ngOnInit() {
    this.getCuentasById(this.activatedRoute.snapshot.paramMap.get('idCuenta'));
  }

  getCuentasById = (arg) => {
    this.cuentasService.getCuentasById(arg).then((res: any) => {
      this.objectDetalle = res;
      this.alias = res.alias;
      this.getTrasferenciaByIdCuenta(res.id);
    });
  };

  getTrasferenciaByIdCuenta = (arg) => {
    this.transferenciasService.getTrasferenciaByIdCuenta(arg).then((res) => {
      this.objectTrasferencias = res;
    });
  };

  // changes
  changeActualizarAlias = ({ target: { value } }) => {
    this.isUpdatAlias = this.objectDetalle.alias !== value ? true : false;
  };

  // onPress
  onActualizarAlias = async (arg) => {
    const loading = await this.loadingController.create({
      message: 'Actualizando alias de la cuenta',
    });
    await loading.present();

    setTimeout(() => {
      this.isUpdatAlias = false;
      this.objectDetalle.alias = arg;
      loading.dismiss();
      // Swal.fire({
      //   icon: 'success',
      //   text: 'Se ha actualizado correctamente el alias',
      //   showConfirmButton: false,
      // });

      Swal.fire({
        icon: 'info',
        text: `El alias ya se encuentra utilizado, intentar con uno diferente`,
        showConfirmButton: false,
      });

      Swal.fire({
        icon: 'warning',
        text: `se presento un problema, no te alertes, espera unos segundos y vulve a intentar`,
        showConfirmButton: false,
      });
      // this.navController.back();
    }, 2000);
  };

  onGenerarQr = async () => {
    const generarQr = await this.modalController.create({
      component: GenerarQrModPage,
      cssClass: 'generarQr',
      mode: 'md',
      componentProps: { cuenta: this.objectDetalle },
    });
    await generarQr.present();

    const { data } = await generarQr.onWillDismiss();

    console.log(data);
  };
}
