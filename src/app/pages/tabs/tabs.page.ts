import { Component } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { QrModPage } from 'src/app/modals/qr-mod/qr-mod.page';
import { TransferenciasModPage } from 'src/app/modals/transferencias-mod/transferencias-mod.page';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
})
export class TabsPage {
  constructor(
    private navController: NavController,
    private modalController: ModalController
  ) {}

  onNavegacion = (arg) => {
    this.navController.navigateForward(arg);
  };

  onGenerarCrearQr = async () => {
    const qrModal = await this.modalController.create({
      component: QrModPage,
      cssClass: 'qrModal',
      mode: 'ios',
      swipeToClose: true,
      backdropDismiss: false,
    });
    await qrModal.present();

    const { data } = await qrModal.onWillDismiss();

    console.log(data);
  };

  onTransferencias = async () => {
    const transferenciaModal = await this.modalController.create({
      component: TransferenciasModPage,
      cssClass: 'transferenciaModal',
      mode: 'ios',
      swipeToClose: true,
      backdropDismiss: false,
    });
    await transferenciaModal.present();

    const { data } = await transferenciaModal.onWillDismiss();

    console.log(data);
  };
}
