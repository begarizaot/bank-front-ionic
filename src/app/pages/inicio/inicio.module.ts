import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InicioPageRoutingModule } from './inicio-routing.module';

import { InicioPage } from './inicio.page';

import { ComponentsModule } from 'src/app/components/components.module';

// modals
import { GenerarQrModPage } from 'src/app/modals/generar-qr-mod/generar-qr-mod.page';
import { GenerarQrModPageModule } from 'src/app/modals/generar-qr-mod/generar-qr-mod.module';
import { QrModPage } from 'src/app/modals/qr-mod/qr-mod.page';
import { QrModPageModule } from 'src/app/modals/qr-mod/qr-mod.module';
import { TransferenciasModPage } from 'src/app/modals/transferencias-mod/transferencias-mod.page';
import { TransferenciasModPageModule } from 'src/app/modals/transferencias-mod/transferencias-mod.module';
import { CuentasModPage } from 'src/app/modals/cuentas-mod/cuentas-mod.page';
import { CuentasModPageModule } from 'src/app/modals/cuentas-mod/cuentas-mod.module';

@NgModule({
  entryComponents: [
    // modals
    GenerarQrModPage,
    QrModPage,
    TransferenciasModPage,
    CuentasModPage,
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InicioPageRoutingModule,
    ComponentsModule,
    // modals
    GenerarQrModPageModule,
    QrModPageModule,
    TransferenciasModPageModule,
    CuentasModPageModule,
  ],
  declarations: [InicioPage],
})
export class InicioPageModule {}
