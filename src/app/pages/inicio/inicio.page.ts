import { Component, OnInit } from '@angular/core';
import { CuentasService } from 'src/app/controllers/cuentas.service';
import { UsuarioStorageService } from 'src/app/storages/usuario-storage.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  objectUsuario: any = {};

  constructor(
    public cuentasService: CuentasService,
    private usuarioStorage: UsuarioStorageService
  ) {}

  ngOnInit() {
    this.storageCargarUsuario();
    this.cuentasService.getListadoDeCuentas();
  }

  storageCargarUsuario = async () => {
    this.objectUsuario = await this.usuarioStorage.storageCargarUsuario();
  };
}
