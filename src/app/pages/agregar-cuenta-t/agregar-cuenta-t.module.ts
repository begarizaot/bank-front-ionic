import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarCuentaTPageRoutingModule } from './agregar-cuenta-t-routing.module';

import { AgregarCuentaTPage } from './agregar-cuenta-t.page';

import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarCuentaTPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [AgregarCuentaTPage],
})
export class AgregarCuentaTPageModule {}
