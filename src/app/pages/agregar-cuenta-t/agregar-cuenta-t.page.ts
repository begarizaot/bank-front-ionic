import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { CuentasService } from 'src/app/controllers/cuentas.service';
import { GeneralesService } from 'src/app/controllers/generales.service';
import { UsuarioStorageService } from 'src/app/storages/usuario-storage.service';
import { validateInput, validateNumber } from 'src/app/utils/utils';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-cuenta-t',
  templateUrl: './agregar-cuenta-t.page.html',
  styleUrls: ['./agregar-cuenta-t.page.scss'],
})
export class AgregarCuentaTPage implements OnInit {
  objectAgregar: any = {};
  objectError: any = {};

  listSelects: any = {};

  objectUsuario: any = {};

  constructor(
    private generalesService: GeneralesService,
    private cuentasService: CuentasService,
    public loadingController: LoadingController,
    private navController: NavController,
    private usuarioStorage: UsuarioStorageService
  ) {}

  ngOnInit() {
    this.getEntidadesBancarias();
    this.getTiposDeCuenta();
    this.getMonedas();
    this.storageCargarUsuario();
  }

  storageCargarUsuario = async () => {
    this.objectUsuario = await this.usuarioStorage.storageCargarUsuario();
  };

  getEntidadesBancarias = async () => {
    const listEntidadBanc = await this.generalesService.getEntidadesBancarias();
    this.listSelects = { ...this.listSelects, listEntidadBanc };
  };

  getTiposDeCuenta = async () => {
    const listTipoCuentas = await this.generalesService.getTiposDeCuenta();
    this.listSelects = { ...this.listSelects, listTipoCuentas };
  };

  getMonedas = async () => {
    const listMonedas = await this.generalesService.getMonedas();
    this.listSelects = { ...this.listSelects, listMonedas };
  };

  // changes
  changeValidarNumeroCuenta = ({ target: { value } }) => {
    const validar = validateNumber(value);
    if (validar) {
      this.objectError.numCuenta = validar;
      return;
    }

    const minValue = validateInput(value, 11);
    if (minValue) {
      this.objectError.numCuenta = minValue;
      return;
    }

    this.objectError.numCuenta = '';
  };

  changeValidarIdentificacion = ({ target: { value } }) => {
    const validar = validateNumber(value);
    if (validar) {
      this.objectError.identificacion = validar;
      return;
    }

    const minValue = validateInput(value, 7);
    if (minValue) {
      this.objectError.identificacion = minValue;
      return;
    }

    this.objectError.identificacion = '';
  };

  // onPress
  onRegistrarCuenta = async (arg) => {
    if (this.handleValidarInformacionCompleta()) {
      this.objectAgregar.isInvalido = true;
      return;
    }

    const loading = await this.loadingController.create({
      message: 'Agregando cuenta de tercero',
    });
    await loading.present();

    this.cuentasService
      .postCrearCuentaTercero({ ...arg, id_usuario: this.objectUsuario.id })
      .then((res) => {
        loading.dismiss();
        Swal.fire({
          icon: 'success',
          text: 'Se ha agregado exitosamente la cuenta',
          showConfirmButton: false,
        });
        this.cuentasService.getListadoDeCuentas();
        this.navController.back();
      })
      .catch(({ db, messenger }) => {
        loading.dismiss();
        Swal.fire({
          icon: db ? 'error' : 'info',
          text: messenger,
          showConfirmButton: false,
        });
      });
  };
  // handles
  handleValidarInformacionCompleta = () => {
    return (
      !this.objectAgregar.alias ||
      !this.objectAgregar.numCuenta ||
      !this.objectAgregar.identificacion ||
      !this.objectAgregar.entidadBanc ||
      !this.objectAgregar.tipoCuenta ||
      !this.objectAgregar.moneda ||
      this.objectError.numCuenta ||
      this.objectError.identificacion
    );
  };
}
