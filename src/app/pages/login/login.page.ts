import { Component, OnInit } from '@angular/core';
import { GeneralesService } from 'src/app/controllers/generales.service';

import {
  InAppBrowser,
  InAppBrowserOptions,
} from '@ionic-native/in-app-browser/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { validateNumber } from 'src/app/utils/utils';
import { LoadingController, NavController } from '@ionic/angular';
import { UsuarioService } from 'src/app/controllers/usuario.service';
import { UsuarioStorageService } from 'src/app/storages/usuario-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  objectSignIn: any = {};
  objectError: any = {};

  listTipoIdentificacion: any = [];

  SliderLogin = [
    {
      icono: 'chatbubble-outline',
      titulo: 'Hablar con un asesor',
      link: 'https://api.whatsapp.com/send?phone=+573015444439&text=Hola,%20Brayan%20Garizao%20Nos%20gusto%20tu%20aplicación',
      blank: true,
    },
    {
      icono: 'logo-linkedin',
      titulo: 'Canal de información',
      link: 'https://www.linkedin.com/in/brayan-garizao-t/',
      blank: true,
    },
    {
      icono: 'call-outline',
      titulo: 'Llamar a la servilínea',
      onPress: () => this.callNumber.callNumber('+573015444439', true),
    },
  ];

  slideOpts = {
    slidesPerView: 2.5,
    speed: 400,
    breakpoints: {
      480: {
        slidesPerView: 2.5,
      },
      640: {
        slidesPerView: 3,
      },
    },
  };

  constructor(
    private generalesService: GeneralesService,
    private iab: InAppBrowser,
    private callNumber: CallNumber,
    public loadingController: LoadingController,
    private navController: NavController,
    private usuarioService: UsuarioService,
    private usuarioStorageService: UsuarioStorageService
  ) {}

  ngOnInit() {
    this.getTiposDeIdentificacion();
  }

  getTiposDeIdentificacion = async () => {
    this.listTipoIdentificacion =
      await this.generalesService.getTiposDeIdentificacion();
    this.objectSignIn.tipoDocumento = 1;
  };

  // changes
  changeValidarFormato = ({ target: { value } }) => {
    const validar = validateNumber(value);
    if (validar) {
      this.objectError.documento = validar;
    } else {
      this.objectError.documento = this.objectSignIn.documento
        ? ''
        : 'Ingresa la información solicitada';
    }
  };

  // onPress
  onIniciarSesion = async (arg) => {
    if (this.handleValidarInformacionCompleta()) {
      this.objectSignIn.isInvalido = true;
      return;
    }

    const loading = await this.loadingController.create({
      message: 'Buscando información',
    });
    await loading.present();

    this.usuarioService
      .postLogin(arg)
      .then(async (res) => {
        await this.usuarioStorageService.storageGuardarUsuario(res);
        loading.dismiss();
        this.navController.navigateRoot('tabs');
      })
      .catch((err) => {
        loading.dismiss();
      });
  };

  onNavegacionCard = ({ link }) => {
    this.iab.create(link, '_system');
  };

  // handle
  handleValidarInformacionCompleta = () => {
    return (
      !this.objectSignIn.documento ||
      !this.objectSignIn.password ||
      this.objectError.documento
    );
  };
}
