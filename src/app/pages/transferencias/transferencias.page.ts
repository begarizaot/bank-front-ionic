import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TransferenciasService } from 'src/app/controllers/transferencias.service';

@Component({
  selector: 'app-transferencias',
  templateUrl: './transferencias.page.html',
  styleUrls: ['./transferencias.page.scss'],
})
export class TransferenciasPage implements OnInit {
  cuenta: any = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    public transferenciasService: TransferenciasService
  ) {}

  ngOnInit() {
    const cuenta = this.activatedRoute.snapshot.paramMap.get('cuenta');
    this.cuenta = cuenta;
    this.getTrasferenciaByCuenta(cuenta);
  }

  getTrasferenciaByCuenta = (arg) => {
    this.transferenciasService.getTrasferenciaByCuenta(arg);
  };
}
