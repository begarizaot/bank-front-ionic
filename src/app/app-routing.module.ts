import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoadinCanActivateService } from './canActivate/loadin-can-activate.service';

const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () =>
      import('./pages/tabs/tabs.module').then((m) => m.TabsPageModule),
  },
  {
    path: '',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
    canActivate: [LoadinCanActivateService],
  },
  {
    path: 'agregarCuentaT',
    loadChildren: () =>
      import('./pages/agregar-cuenta-t/agregar-cuenta-t.module').then(
        (m) => m.AgregarCuentaTPageModule
      ),
  },
  {
    path: 'detalleCuenta/:idCuenta',
    loadChildren: () =>
      import('./pages/detalle-cuenta/detalle-cuenta.module').then(
        (m) => m.DetalleCuentaPageModule
      ),
  },
  {
    path: 'generarQr',
    loadChildren: () =>
      import('./pages/generar-qr/generar-qr.module').then(
        (m) => m.GenerarQrPageModule
      ),
  },
  {
    path: 'transferir/:cuenta',
    loadChildren: () =>
      import('./pages/transferir/transferir.module').then(
        (m) => m.TransferirPageModule
      ),
  },
  {
    path: 'transferencias/:cuenta',
    loadChildren: () =>
      import('./pages/transferencias/transferencias.module').then(
        (m) => m.TransferenciasPageModule
      ),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
