import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'headerComp',
  templateUrl: './header-comp.component.html',
  styleUrls: ['./header-comp.component.scss'],
})
export class HeaderCompComponent implements OnInit {
  @Input() btnBack: boolean = false;
  @Input() btnTrasferencia: any = '';
  @Input() titulo: any = '';

  constructor(private navController: NavController) {}

  ngOnInit() {}

  onButtonBack = () => {
    this.navController.back();
  };

  onCrearTrasferencia = () => {
    this.navController.navigateForward(`/transferir/${this.btnTrasferencia}`);
  };
}
