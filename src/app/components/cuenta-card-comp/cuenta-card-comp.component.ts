import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cuentaCardComp',
  templateUrl: './cuenta-card-comp.component.html',
  styleUrls: ['./cuenta-card-comp.component.scss'],
})
export class CuentaCardCompComponent implements OnInit {
  @Input() informacion: any = {};

  constructor() {}

  ngOnInit() {}
}
