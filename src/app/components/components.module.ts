import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { CuentaPrivadPipe } from 'src/app/pipes/cuenta-privad.pipe';

import { HeaderCompComponent } from './header-comp/header-comp.component';
import { CuentaCardCompComponent } from './cuenta-card-comp/cuenta-card-comp.component';
import { TransferenciasCardCompComponent } from './transferencias-card-comp/transferencias-card-comp.component';

@NgModule({
  declarations: [
    HeaderCompComponent,
    CuentaCardCompComponent,
    TransferenciasCardCompComponent,
    // pipes
    CuentaPrivadPipe,
  ],
  exports: [
    HeaderCompComponent,
    CuentaCardCompComponent,
    TransferenciasCardCompComponent,
  ],
  imports: [CommonModule, IonicModule, FormsModule, RouterModule],
})
export class ComponentsModule {}
