import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'transferenciasCardComp',
  templateUrl: './transferencias-card-comp.component.html',
  styleUrls: ['./transferencias-card-comp.component.scss'],
})
export class TransferenciasCardCompComponent implements OnInit {
  @Input() informacion: any = {};

  constructor() {}

  ngOnInit() {}
}
