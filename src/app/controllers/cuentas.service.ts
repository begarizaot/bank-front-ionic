import { Injectable } from '@angular/core';
import { CuentasServService } from '../services/cuentas-serv.service';
import { UsuarioStorageService } from '../storages/usuario-storage.service';

@Injectable({
  providedIn: 'root',
})
export class CuentasService {
  objectListCuentas: any = {};
  objectLoading: any = { cuentas: true };

  constructor(
    private cuentasServService: CuentasServService,
    private usuarioStorage: UsuarioStorageService
  ) {}

  getListadoDeCuentas = () => {
    return new Promise((resolve, reject) => {
      this.objectLoading.cuentas = true;
      this.usuarioStorage.storageCargarUsuario().then(({ id }) => {
        this.cuentasServService.getCuentasByIdUsuario(id).subscribe((res) => {
          this.objectLoading.cuentas = false;
          this.objectListCuentas = res;
          resolve(res);
        });
      });
    });
  };

  getCuentasById = (id) => {
    return new Promise((resolve, reject) => {
      this.cuentasServService.getCuentasById(id).subscribe((res) => {
        resolve(res);
      });
    });
  };

  postCrearCuentaTercero = (data) => {
    return new Promise((resolve, reject) => {
      this.cuentasServService
        .postCrearCuentaTercero(data)
        .subscribe(({ state, data, messenger, db }) => {
          if (state) {
            reject({ messenger, db });
          } else {
            resolve(data);
          }
        });
    });
  };
}
