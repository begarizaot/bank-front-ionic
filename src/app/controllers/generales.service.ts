import { Injectable } from '@angular/core';
import { GeneralesServService } from '../services/generales.service';

@Injectable({
  providedIn: 'root',
})
export class GeneralesService {
  constructor(private generalesServService: GeneralesServService) {}

  getTiposDeIdentificacion = () => {
    return new Promise((resolve, reject) => {
      this.generalesServService.getTiposIdentificacion().subscribe(
        (res) => {
          resolve(res);
        },
        (err) => {}
      );
    });
  };

  getEntidadesBancarias = () => {
    return new Promise((resolve, reject) => {
      this.generalesServService.getEntidadesBancarias().subscribe(
        (res) => {
          resolve(res);
        },
        (err) => {}
      );
    });
  };

  getTiposDeCuenta = () => {
    return new Promise((resolve, reject) => {
      this.generalesServService.getTiposDeCuenta().subscribe(
        (res) => {
          resolve(res);
        },
        (err) => {}
      );
    });
  };

  getMonedas = () => {
    return new Promise((resolve, reject) => {
      this.generalesServService.getMonedas().subscribe(
        (res) => {
          resolve(res);
        },
        (err) => {}
      );
    });
  };
}
