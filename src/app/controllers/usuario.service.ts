import { Injectable } from '@angular/core';
import { UsuarioServService } from '../services/usuario-serv.service';

import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  constructor(private usuarioServService: UsuarioServService) {}

  postLogin = (arg) => {
    return new Promise((resolve, reject) => {
      this.usuarioServService
        .postLogin(arg)
        .subscribe(({ state, data, messenger, db }) => {
          if (state) {
            reject();
            Swal.fire({
              icon: db ? 'error' : 'info',
              text: messenger,
              showConfirmButton: false,
            });
          } else {
            resolve(data);
          }
        });
    });
  };
}
