import { Injectable } from '@angular/core';
import { TransferenciasServService } from '../services/transferencias-serv.service';

@Injectable({
  providedIn: 'root',
})
export class TransferenciasService {
  objectListTasferencias: any = [];
  objectLoading: any = { cuentas: true };

  constructor(private transferenciasServService: TransferenciasServService) {}

  postTrasferencia = (arg) => {
    return new Promise((resolve, reject) => {
      const data = {
        id_usuario: arg.id_usuario,
        id_origen: arg.origen.id,
        id_destino: arg.destino.id,
        id_moneda: arg.moneda,
        valorTraf: arg.valorTraf,
        cuenta: arg.cuenta,
        descripcion: arg.descripcion,
        saldo_origen: arg.origen.saldo,
        saldo_destino: arg.destino.saldo,
      };
      this.transferenciasServService
        .postTrasferencia(data)
        .subscribe(({ state, data, messenger, db }) => {
          if (state) {
            reject({ messenger, db });
          } else {
            resolve(data);
          }
        });
    });
  };

  getTrasferenciaByCuenta = (cuenta) => {
    return new Promise((resolve, reject) => {
      this.objectLoading.transferencia = true;
      this.transferenciasServService
        .getTrasferenciaByCuenta(cuenta)
        .subscribe((res) => {
          this.objectLoading.transferencia = false;
          this.objectListTasferencias = res;
          resolve(res);
        });
    });
  };

  getTrasferenciaByIdCuenta = (idCuenta) => {
    return new Promise((resolve, reject) => {
      this.transferenciasServService
        .getTrasferenciaByIdCuenta(idCuenta)
        .subscribe((res) => {
          resolve(res);
        });
    });
  };
}
