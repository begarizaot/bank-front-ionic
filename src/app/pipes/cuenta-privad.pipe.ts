import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cuentaPrivad',
})
export class CuentaPrivadPipe implements PipeTransform {
  transform(value: any, ...args: unknown[]): unknown {
    if (value) {
      return `****${value.toString().slice(-4)}`;
    }
    return null;
  }
}
