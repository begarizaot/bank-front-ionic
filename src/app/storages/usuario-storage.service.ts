import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root',
})
export class UsuarioStorageService {
	constructor(private storage: Storage) {}

	storageGuardarUsuario(usuario) {
		return new Promise((resolve, reject) => {
			this.storage.set('usuario', usuario);
			resolve('ok');
		});
	}

	async storageCargarUsuario() {
		const buscar = await this.storage.get('usuario');
		return buscar || {};
	}

	async storageBorrarUsuario() {
		return new Promise((resolve, reject) => {
			this.storage.remove(`usuario`);
			resolve('ok');
		});
	}
}
